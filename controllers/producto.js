const Producto = require("../models/producto");
const image = require("../utils/image");

//Crear productos
function crearProducto(req, res) {
  const nuevo_producto = new Producto(req.body);

  //recibe imagenes en la constante imagepath
  const imagePath = image.getFilePath(req.files.imagenProducto);
  nuevo_producto.imagenProducto = imagePath;

  //guardar el producto
  nuevo_producto.save((error, productoAlmacenado) => {
    if (error) {
      res.status(400).send({ msg: "Error al crear el producto" });
    } else {
      res.status(201).send(productoAlmacenado);
    }
  });
}
//obtención de los productos
function obtenerProductos(req, res) {
  //esta validación en caso que no me llegue nada por defecto page 1 y limite 10 productos
  const { page = 1, limit = 10 } = req.query;

  const options = {
    //convertimos de string a entero
    page: parseInt(page),
    limit: parseInt(limit),
  };

  // este Producto viene del import del models/producto
  Producto.paginate({}, options, (error, productos) => {
    if (error) {
      res.status(400).send({ msg: "Error al obtener los productos" });
    } else {
      res.status(200).send(productos);
    }
  });
}

module.exports = {
  crearProducto,
  obtenerProductos,
};
