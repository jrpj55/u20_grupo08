Proyecto petschop

1. Desarrollo del Backend
   1.1. Se crea una carpeta llamada BACKEND
   1.1.1. Creamos el archivo package.json con el comando yarn init y me pide los datos iniciales del proyecto.
   1.1.2. Creamos una carpeta llamada controllers donde van a ir las funciones que se ejecutan cuando se llama una petición http.
   1.1.3. Creamos una carpeta llamada middlewares donde van las funciones en mitad de una petición para validar si el usuario esta validado, subir imágenes, etc.
   1.1.4. Creamos la carpeta models que es donde van a ir todos los modelos de las tablas de la base de datos
   1.1.5. Creamos la carpeta router que donde van a ir las rutas.
   1.1.6. Creamos la carpeta uploads que es donde van a subir todas nuestras imágenes y ficheros desde mi aplicación
   1.1.7. Creamos una carpeta utils donde van todas las utilidades que se vayan creando en el cual se vaya avanzando con nuestra aplicación
   1.1.8. Crear nuestra base de datos en mongodb: página web mongodb.com, nos registramos y luego nos logueamos y creamos la BD con el nombre de petshopdb. Luego se crea un usuario=userdev y el paswword=userdevu20g08, damos el rol de admin y la ip para que todos puedan acceder “ALLOWS ACCES ..”
   1.1.9. Conexión de la base de datos con nuestra aplicación:
   1.1.9.1. Instalamos la dependencia mongoose en nuestra aplicación: yarn add mongoose esta permite ejecutar y controlar nuestra base de datos en la aplicación. Revisamos package.json y revisamos en dependencias la versión instalada.
   1.1.9.2. Creamos un fichero js para guardar constantes, llamado constants.js dentro del directorio BACKEND y creamos las variables con los datos de la base de datos.
   1.1.9.3. Creamos el fichero index.js y se realiza la conexión a la base de datos.
   1.1.10. Crear el servidor HTTP con express:
   1.1.10.1. Instalamos la dependencia express: yarn add express
   1.1.11. Creamos un nuevo fichero llamado app.js en este fichero va ir toda la configuración de express.
   Nota: instalamos la dependencia nodemon para recargar la conexión cada vez de allá cambios y no tengamos que parar para cada actualización, entonces le decimos yarn add nodemon y enter .
   1.1.12. Vamos a configurar la dependencia Body-parse que pertenece a express, es lo que nos llega a nosotros del cliente al servidor, por ejemplo cuando el cliente en el body quiere mandar un json con datos como nombre, apellido de un nuevo registro y se parsea para que llegue a nuestro servidor, instalamos el paquete: yarn add body-parse.
   1.1.13. Configuramos los Cors que pertece a express: configuramos los header de nuestro servidor para cuando hagamos peticiones de nuestro cliente el servidor no las bloquee, sino que las deje pasar: instalamos la dependencia cors: yarn add cors.
   1.1.14. Vamos a crear nuestros endpoint: vamos a crear el controlador de la autenticación.js, creamos dentro de la carpeta controller el fichero autenticación.js
   1.1.15. Despues vamos a crear las rutas de autenticación: entramos a la carpeta router y creamos el fichero autenticación.js
   1.1.16. Se utiliza un simulador del backend llamado Insomnia o Postman y se descargan de las respectivas páginas.
   1.1.17. Vamos a crear el modelo de usuario: se crea un fichero en la carpeta models llamado usuario.js
   1.1.18. Vamos a registrar el usuario: en el autenticación.js del controller,
   1.1.19. Instalamos la dependencia bcryptjs para encriptar la contraseña del usuario, decimos yarn add bcryptjs.
   1.1.20. AccesToken y refrehtToken me debe devolver al cliente para poder generar una sesión: instalamos la dependencia jsonwebtoken para generar los tokens, entonces yarn add jsonwebtoken.
   1.1.21. Vamos a la carpeta utils y creamos un fichero llamado jwt.js todo lo que tenga relación con jsonwebtoken va ir programado en ese fichero.
   1.1.22. Vamos a loguearnos: creamos el endponit para el logueo
   1.1.23. La estructura de los endpoint de usuarios: función usuarioLogueado: devuelve los datos del usuario que se encuentra logueado.
   1.1.24. Descargamos el paquete connect-multiparty que sirve para guardar las rutas de las imágenes para poderlas ver o listar, yarn add connect-multiparty.
   1.1.25. Para subir y ver imágenes creamos un fichero llamado image.js dentro de la carpeta utils, donde cargamos lo que anexemos. Permite ver la imagen.
   1.1.26. Ahora vamos a crear el modelo de productos en la carpeta models u fichero llamado producto.js.
   1.1.27. Creamos el fichero de producto js dentro de la carpeta controlles con las función de crear y obtener todos los productos.
   1.2.28. Se creo el fichero producto.js dentro de la carpetas router para definir la ruta, igual manera se actaulizo en la app.js
