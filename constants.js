const DB_USER = "userdev";
const DB_PASSWORD = "userdevu20g08";
const DB_HOST = "misiontic-u20-g08.9epwem9.mongodb.net";

const API_VERSION = "v1";
const IP_SERVER = "localhost";

const JWT_SECRET_KEY = "99hh99yyggff54rrsakkjhgfd";

module.exports = {
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  API_VERSION,
  IP_SERVER,
  JWT_SECRET_KEY,
};
