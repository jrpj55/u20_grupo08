const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { API_VERSION } = require('./constants');

const app = express();

// Importación de las rutas
//controller autenticacion - routes autenticacion y por ultimo al app.js
const autenticacionRutas = require('./router/autenticacion');
const usuariosRoutes = require('./router/usuarios');
//const menuRoutes = require("./routes/menu");
const productoRoutes = require('./router/producto');
const ventaRoutes = require('./router/venta');
//const newsletterRoutes = require("./routes/newsletter");

// Configure Body Parser para enviar contenido del body json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Configure static folder uploads. Con esta linea me deja acceder en la web
//a todas las imagenes que se encuentra en upload : http://localhost:3900/+ la el nombre de la imagen
app.use(express.static('uploads'));

//Configure Header HTTP - CORS
app.use(cors());

// Configuración de las rutas
app.use(`/api/${API_VERSION}`, autenticacionRutas);
app.use(`/api/${API_VERSION}`, usuariosRoutes);
//app.use(`/api/${API_VERSION}`, menuRoutes);
app.use(`/api/${API_VERSION}`, productoRoutes);
app.use(`/api/${API_VERSION}`, ventaRoutes);
//app.use(`/api/${API_VERSION}`, newsletterRoutes);

module.exports = app;
