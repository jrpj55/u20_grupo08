const mongoose = require("mongoose");
const app = require("./app");
const {
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  IP_SERVER,
  API_VERSION,
} = require("./constants");

const PORT = process.env.POST || 3900;

mongoose.connect(
  `mongodb+srv://${DB_USER}:${DB_PASSWORD}@${DB_HOST}/`, //se trabaja con esas comillas para poder unir string con variables
  (error) => {
    if (error) throw error;
    app.listen(PORT, () => {
      console.log("######################");
      console.log("###### API REST - CONEXIÓN ARRIBA  GRUPO 08 ######");
      console.log("######################");
      console.log(`http://${IP_SERVER}:${PORT}/api/${API_VERSION}`);
    });
  }
);
