const express = require("express");
const multiparty = require("connect-multiparty");
const UserController = require("../controllers/usuarios");
const md_auth = require("../middlewares/authenticated");

const md_upload = multiparty({ uploadDir: "./uploads/fotos" }); //guardar imagenes a esa ruta
const api = express.Router();

api.get(
  "/user/me",
  [md_auth.autenticacionSegura],
  UserController.usuarioLogueado
);
api.get(
  "/users",
  [md_auth.autenticacionSegura],
  UserController.obtenerTodosUsuarios
);
api.post(
  "/user/create",
  [md_auth.autenticacionSegura, md_upload],
  UserController.crearUsuario
);
api.patch(
  "/user/:id_usuario",
  [md_auth.autenticacionSegura, md_upload],
  UserController.actualizarUsuario
);
api.delete(
  "/user/:id",
  [md_auth.autenticacionSegura],
  UserController.eliminarUsuario
);

module.exports = api;
