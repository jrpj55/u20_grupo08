const express = require("express");
const multiparty = require("connect-multiparty");
const UserController = require("../controllers/producto");
const md_auth = require("../middlewares/authenticated");

const md_upload = multiparty({ uploadDir: "./uploads/imageProduct" }); //guardar imagenes a esa ruta
const api = express.Router();

api.get("/productos", UserController.obtenerProductos);
api.post(
  "/producto/create",
  [md_auth.autenticacionSegura, md_upload],
  UserController.crearProducto
);

module.exports = api;
