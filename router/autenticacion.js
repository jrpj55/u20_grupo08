const express = require("express");
const AuthController = require("../controllers/autenticacion");

const api = express.Router();

//cuando se envpian datos del cliente al servidor se utiliza post
//cundo se envía la ruta /auth/register me devuelve la función register que se encuentra en controllers/auth
//que fue asignado a la constante AuthController
api.post("/auth/register", AuthController.registrarUsuario);
api.post("/auth/login", AuthController.login);
api.post("/auth/refresh_access_token", AuthController.refreshAccessToken);

module.exports = api;
