const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const EsquemaProducto = mongoose.Schema({
  nombreProducto: String,
  precioProducto: Number,
  imagenProducto: String,
  inventario: Number,
});

EsquemaProducto.plugin(mongoosePaginate);
module.exports = mongoose.model("Producto", EsquemaProducto);
