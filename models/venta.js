const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const EsquemaVenta = new mongoose.Schema(
  {
    ventaItems: [
      {
        nombreProducto: { type: String, required: true },
        contidad: { type: Number, required: true },
        imagen: { type: String, required: true },
        precio: { type: Number, required: true },
        producto: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Producto',
          required: true,
        },
      },
    ],
    direccionEnvio: {
      nombreCompleto: { type: String, required: true },
      direccion: { type: String, required: true },
      ciudad: { type: String, required: true },
      telefono: { type: String, required: true },
    },
    metodoPago: { type: String, required: true },
    resultadoPago: {
      id: String,
      estado: String,
      email: String,
    },
    precioProducto: { type: Number, required: true },
    precioEnvio: { type: Number, required: true },
    precioImpuestos: { type: Number, required: true },
    precioTotal: { type: Number, required: true },
    usuario: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    estadoPago: { type: Boolean, default: false },
    pagadoEn: { type: Date },
    estadoEntregado: { type: Boolean, default: false },
    entregadoEn: { type: Date },
  },
  {
    marcasTiempo: true,
  }
);

EsquemaVenta.plugin(mongoosePaginate);
module.exports = mongoose.model('Venta', EsquemaVenta);
